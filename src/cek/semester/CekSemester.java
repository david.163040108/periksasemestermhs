package cek.semester;

public class CekSemester {

	int semester = 20161;
	int angkatan = 2016;

	public void cek() {
		if (semester % 2 != 0) {
			int a = ((semester + 10) - 1) / 10;
			int b = a - angkatan;
			int c = (b * 2) - 1;
			System.out.println(c);
		} else {
			int a = ((semester + 10) - 2) / 10;
			int b = a - angkatan;
			int c = b * 2;
			System.out.println(c);
		}
	}
	
	public static void main(String[] args) {
		CekSemester cekSemester = new CekSemester();
		
		cekSemester.cek();
	}
}
